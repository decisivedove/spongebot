# SpongeBot

A Discord Bot that can perform administrative tasks and allows users to report each other. Be sure to give the bot admin privellages and create two roles. One of them will be 'Fellow Citizens' and the other one will be 'TheEarthsMantle:BanishedFromRegularSociety'.
You can edit the role names in the code and change stuff accordinglt. By default, 'Fellow Citizens' is assumed to have the power to type and  'TheEarthsMantle:BanishedFromRegularSociety' is expected to have no power. If a user is reported thrice, they burn in Eath's mantle for their sins.

To get this bot, use:` git clone https://gitlab.com/decisivedove/spongebot && cd spongebot`

```
Hi I am SpongeBot. I will make this server great again! I am better than you! I am capable of sending any Fellow Citizen to the earths mantle so respect me!!!
Use !hello to greet me.
Use !joke to read a joke.
Use !clear to clear the chat.
Use !report to !banish people to the earths mantle for their sins. Do not abuse or you shall be banished! e.g. !report @mention_of_annoying_user
Use !troll to post a message anonymously. Your identity will be visible for upto a few seconds and then it would be deleted. 
Use !ping to test latency.
Use !helpme to view this message at any time.
Use !question to ask a yes no question and predict the future.

```

This bot has dependencies. Run the following commands to deal with dependencies before using. This is just a script that can be used with a token code. You have to manually create a bot in Discord and edit the files to enter the token code.

`python3 -m pip install -U discord.py requests`


To run the bot wih python3 bot.py.

This piece of software is under the MIT license. See the license file for more details. 