import discord
import random
import joke_api
from discord.ext import commands
from discord.utils import get

bot = commands.Bot(command_prefix='!')
reported_members = list()
answers = list()
@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print('-> Bot is ready!                              OK!')
    print('-------------------------------------------------')



@bot.event
async def on_member_join(member):
    print(f'{member} has joined the server.')


@bot.event
async def on_member_remove(member):
    print(f'{member} has left the server.')


@bot.command()
async def ping(ctx):
    await ctx.send(f'Pong! {round(bot.latency*1000)}ms')


@bot.command()
async def question(ctx,*, question):
    responses=[
            "It is certain.",
            "It is decidedly so.",
            "Without a doubt.",
            "Yes - definitely.",
            "You may rely on it.",
            "As I see it, yes.",
            "Most likely.",
            "Outlook good.",
            "Yes.",
            "Signs point to yes.",
            "Reply hazy, try again.",
            "Ask again later.",
            "Better not tell you now.",
            "Cannot predict now.",
            "Concentrate and ask again.",
            "Don't count on it.",
            "My reply is no.",
            "My sources say no.",
            "Outlook not so good.",
            "Very doubtful."]
    await ctx.send(f'Question: {question}\nAnswer: {random.choice(responses)}')
    print('Answered a question!')


@bot.command()
async def clear(ctx):
    await ctx.channel.purge()
    print('cleared some messages')

@bot.command()
async def set_answer(ctx, arg):
    print (arg)
    temp = str(arg).split(':')
    number = temp[0]
    answer = temp[1]
    if answers[int(number)] is None:
        answers.insert(int(number), answer)
        await ctx.send('success!')
    else:
        await ctx.send("The answer already exists. Try using the change_answer command if you are sure this answer is correct!")
    print(str(number)+':'+ answer + 'has been set')



@bot.command()
async def change_answer(ctx, arg):
    print(arg)
    temp = str(arg).split(':')
    number = temp[0]
    answer = temp[1]

    answers.insert(int(number), answer)
    await ctx.send("success!")
    print(str(number)+':'+ answer + 'has been ovverided!')


@bot.command()
async def get_answer(ctx, *, number):
    if not int(number) == None:
        await ctx.send( answers[int(number)] )
    else:
        await ctx.send('Sorry answer has not been registered yet. Be the first to use the set_answer command')

@bot.command()
async def joke(ctx):
    joke = joke_api.get_joke()

    if joke == False:
        await ctx.send("Couldn't get joke from API. Try again later.")
    else:
        await ctx.send(joke['setup'] + '\n' + joke['punchline'])


@bot.command()
async def troll(ctx):
    reply = str(ctx.message.system_content)
    await ctx.send(reply[6:])
    await ctx.message.delete()


@bot.command()
async def hello(ctx):
    await ctx.send('Hello')


@bot.command()
async def helpme(ctx):
    await ctx.send(' Hi I am SpongeBot. I will make this server great again! I am better than you! I am capable of sending any Fellow Citizen to the earths mantle so respect me!!! ')
    await ctx.send('Use !hello to greet me.')
    await ctx.send('Use !joke to read a joke.')
    await ctx.send('Use !clear to clear the chat.')
    await ctx.send('Use !report to !banish people to the earths mantle for their sins. Do not abuse or you shall be banished! e.g. !report @mention_of_annoying_user')
    await ctx.send('Use !troll to post a message anonymously. Your identity will be visible for upto a few seconds and then it would be deleted.')
    await ctx.send('Use !ping to test latency.')
    await ctx.send('Use !helpme to view this message at any time.')
    await ctx.send('Use !question to ask a yes no question and predict the future.')


@bot.command()
async def report(ctx, target: discord.Member, *, reason=None):
    reasonForReport = reason
    reported_members.append(target)
    print(target)
    print('Has been reported for :')
    print(reason)
    await ctx.send('The incident has been reported and appropriate action shall be taken. The reported user shall be banished to Earths mantle on the third report.')
    a = 0
    for mebr in reported_members:
        if str(mebr) == str(target):
            a = a +1
    if a == 1:
        print ('First Offense!')
        await ctx.send('First Offense!')
    if a == 2:
        print ('Second Offense!')
        await ctx.send('Second Offense!')
    if a == 3:
        print ('Third Offense!')
        await ctx.send('Third Offense!')
        print('The user is being banished...')
        for role in ctx.guild.roles:
            if role.name == "Fellow Citizens":
                await target.remove_roles(role)
            if role.name == 'TheEarthsMantle:BanishedFromRegularSociety':
                await target.add_roles(role)
        # roles = discord.Guild.fetch_roles()
        # print(roles)
        await ctx.send('The reported user has been banished to earths mantle to burn and to be set to fire for their sins. If you are abusing this, you shall burn too. With anarchy comes responsibility. If this was a mistake, contact a mod immediately')




for i in range(300):
    answers.append(None)

bot.run('Token Code Here!')
